import UIKit
import RSSelectionMenu

protocol EDTextFieldDelegate {
    func pickerSelected(atIndex:Int, tag :Int)
}
class EDTextField: UIView {

    var view: UIView!
    
    
    @IBOutlet weak var bottomLine: UIView!
    @IBOutlet weak var dropDownArrow: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var textField: UITextField!
     @IBOutlet weak var dropDownButton: UIButton!
    @IBOutlet weak var textLbl: UILabel!
    @IBOutlet weak var iconPadding: NSLayoutConstraint!
    @IBOutlet weak var widthIcon: NSLayoutConstraint!
    
    @IBOutlet weak var errorIcon: UIImageView!
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var showPasswordBtn: UIButton!
    
    let datePicker = UIDatePicker()
    var pickerView = UIPickerView()
    var selectedIndex = 0
    var pickerModel = [String]()
     var selectedDataArray = [String]()
     var isSearchAblePicker = false
    var isMultiSelection = false
    var pickerText = ""
    var isDatePicker = false
    var isDateTimePicker = false
    var isPicker = false
    var selected_icon: String!
    var placeHolderColorValue: UIColor?
    var delegate:EDTextFieldDelegate?
    var maxDate = NSDate() as Date
    var minDate = NSDate() as Date
    
    @IBOutlet weak var iconConstraint: DynamicVerticalConstraint!
    
    @IBOutlet weak var iconConstraint2: DynamicVerticalConstraint!
    
    @IBOutlet weak var iconConstraintThree: DynamicVerticalConstraint!
    @IBOutlet weak var separator: UIImageView!
    
    @IBInspectable var titleValue: String? {
        
        get{
            return titleLabel.text
        }
        
        set(fieldValue){
            titleLabel.text = fieldValue
        }
    }
    
    
    
    @IBInspectable var selectedIcon: String? {
        
        get{
            return self.selected_icon
        }
        
        set(fieldValue){
            self.selected_icon = fieldValue
        }
    }
    
    @IBInspectable var placeHolderString: String?{
        
        get{
            return textField.placeholder
        }
        
        set(placeHolderString){
            textField.placeholder = placeHolderString
           // textLbl.text = placeHolderString
        }
    }

    
    @IBInspectable var placeHolderColor: UIColor? {
        get {
            return placeHolderColorValue
        }
        set(newValue) {
            self.placeHolderColorValue = newValue
            self.textField.attributedPlaceholder = NSAttributedString(string:self.textField.placeholder != nil ? self.textField.placeholder! : textField.placeholder!, attributes:[NSAttributedString.Key(rawValue: NSAttributedString.Key.foregroundColor.rawValue): newValue!
                ] as [NSAttributedString.Key: UIColor])
        }
    }
    @IBInspectable var showBottomLine: Bool{
        
        get{
            return bottomLine.isHidden
        }
        
        set(value){
            bottomLine.isHidden = !value
        }
    }
    @IBInspectable var hideIcon: Bool{
        
        get{
            return icon.isHidden
        }
        
        set(value){
            icon.isHidden = value
            if(value){
                iconConstraint.constant = 0
                iconConstraint2.constant = 0
                separator.isHidden = true
                iconConstraintThree.constant = 4
            }
            }
    }
    
    @IBInspectable var showCalender: Bool {
        get {
            return isDatePicker
        }
        set(iscalender) {
            if(iscalender){
                isDatePicker = true
                
            }else{
                isDatePicker = false
                
            }
            
        }
    }
    @IBInspectable var dateTimePicker: Bool {
        get {
            return isDateTimePicker
        }
        set(iscalender) {
            if(iscalender){
                isDateTimePicker = true
                
            }else{
                isDateTimePicker = false
                
            }
            
        }
    }
    @IBInspectable var isSecure: Bool{
        
        get{
            return textField.isSecureTextEntry
        }
        
        set(isSecure){
            if(isSecure){
                textField.isSecureTextEntry = true
                showPasswordBtn.isHidden = false
                showPasswordBtn.addTarget(self, action: #selector(EDTextField.secureUnsecure), for: .touchUpInside)
            }else{
                textField.isSecureTextEntry = false
                showPasswordBtn.isHidden = true
                showPasswordBtn.removeTarget(self, action:#selector(EDTextField.secureUnsecure), for: .touchUpInside)
            }
        }
    }
    
    @IBInspectable var isDropDown: Bool{
        
        get{
            return dropDownArrow.isHidden
        }
        
        set(isDropDown){
            if(isDropDown){
                textField.tintColor = UIColor.clear
                textField.isEnabled = false
                dropDownArrow.isHidden = false
                dropDownButton.isHidden = false
            }else{
                textField.isEnabled = true
                dropDownArrow.isHidden = true
                dropDownButton.isHidden = true
            }
        }
    }

    @IBInspectable var SearchAblePicker: Bool {
        get {
            return isSearchAblePicker
        }
        set(iscalender) {
            if(iscalender){
                isSearchAblePicker = true
                
            }else{
                isSearchAblePicker = false
                
            }
            
        }
    }
    @IBInspectable var MUltipleSelection: Bool {
        get {
            return isMultiSelection
        }
        set(iscalender) {
            if(iscalender){
                isMultiSelection = true
                
            }else{
                isMultiSelection = false
                
            }
            
        }
    }
    
    @IBInspectable var fieldIcon: UIImage?{
        
        get{
            return icon.image
        }
        
        set(fieldIcon){
            if(fieldIcon != nil){
                icon.image = fieldIcon
            }
        }
    }
    
    func showDateTimePicker(){
        //Formate Date
        
        datePicker.datePickerMode = .dateAndTime
       // datePicker.timeZone = TimeZone(abbreviation: "UTC")
        datePicker.minimumDate = minDate
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        
        //done button & cancel button
        let doneButton = UIBarButtonItem(title: "Select", style: .plain, target: self, action: #selector(donedateTimePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker))
        let titleButton = UIBarButtonItem(title: self.placeHolderString, style: .plain, target: nil, action: nil)
        titleButton.isEnabled = false
        titleButton.tintColor = UIColor.lightGray
        toolbar.setItems([cancelButton,spaceButton,titleButton,spaceButton,doneButton], animated: false)
        toolbar.backgroundColor = UIColor.white
        // add toolbar to textField
        textField.inputAccessoryView = toolbar
        // add datepicker to textField
        textField.inputView = datePicker
        textField.becomeFirstResponder()
    }
    
    @objc func donedateTimePicker(){
        //For date formate
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
      //  formatter.timeZone = TimeZone(abbreviation: "UTC")
        textField.text = formatter.string(from: datePicker.date)
        //dismiss date picker dialog
        
        self.view.endEditing(true)
    }
    
    @objc func secureUnsecure(){
        if(textField.isSecureTextEntry){
            textField.isSecureTextEntry = false
            showPasswordBtn.setTitle("Hide", for: .normal)
        }else{
            textField.isSecureTextEntry = true
            showPasswordBtn.setTitle("Show", for: .normal)
        }
    }
    
    
    func showDatePicker(){
        //Formate Date
        
        datePicker.datePickerMode = .date
        datePicker.maximumDate = maxDate
        datePicker.minimumDate = minDate
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        
        //done button & cancel button
        let doneButton = UIBarButtonItem(title: "Select", style: .plain, target: self, action: #selector(donedatePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker))
        let titleButton = UIBarButtonItem(title: self.placeHolderString, style: .plain, target: nil, action: nil)
        titleButton.isEnabled = false
        titleButton.tintColor = UIColor.lightGray
        toolbar.setItems([cancelButton,spaceButton,titleButton,spaceButton,doneButton], animated: false)
        toolbar.backgroundColor = UIColor.white
        // add toolbar to textField
        textField.inputAccessoryView = toolbar
        // add datepicker to textField
        textField.inputView = datePicker
        textField.becomeFirstResponder()
    }
    
    @objc func donedatePicker(){
        //For date formate
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
       // formatter.timeZone = TimeZone(abbreviation: "UTC")
        textField.text = formatter.string(from: datePicker.date)
        //dismiss date picker dialog
        
        self.view.endEditing(true)
    }
    @objc func cancelDatePicker(){
        //cancel button dismiss datepicker dialog
        self.view.endEditing(true)
    }
    
    //Time Picker
    
    func showTimePicker(){
        //Formate Date
        
        datePicker.datePickerMode = .time
      //  datePicker.locale = NSLocale(localeIdentifier: "en_GB") as Locale
        
        
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        
        //done button & cancel button
        let doneButton = UIBarButtonItem(title: "Select", style: .plain, target: self, action: #selector(doneTimePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelTimePicker))
        let titleButton = UIBarButtonItem(title: self.placeHolderString, style: .plain, target: nil, action: nil)
        titleButton.isEnabled = false
        titleButton.tintColor = UIColor.lightGray
        toolbar.setItems([cancelButton,spaceButton,titleButton,spaceButton,doneButton], animated: false)
        toolbar.backgroundColor = UIColor.white
        // add toolbar to textField
        textField.inputAccessoryView = toolbar
        // add datepicker to textField
        textField.inputView = datePicker
    }
    
    @objc func doneTimePicker(){
        //For date formate
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm"
        
        textField.text = formatter.string(from: datePicker.date)
        //dismiss date picker dialog
        
        self.view.endEditing(true)
    }
    
    
    
    @objc  func cancelTimePicker(){
        //cancel button dismiss datepicker dialog
        self.view.endEditing(true)
    }
    
    func showGenericPickerView(){
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        
        //done button & cancel button
        let doneButton = UIBarButtonItem(title: "Select", style: .plain, target: self, action: #selector(EDTextField.donePicker))
        doneButton.tintColor = UIColor.black
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let titleButton = UIBarButtonItem(title: self.placeHolderString, style: .plain, target: nil, action: nil)
        titleButton.isEnabled = false
        titleButton.tintColor = UIColor.lightGray
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(EDTextField.cancelPicker))
       
        cancelButton.tintColor = UIColor.black
        toolbar.setItems([cancelButton,spaceButton,titleButton,spaceButton,doneButton], animated: false)
        toolbar.backgroundColor = UIColor.white
        textField.inputAccessoryView = toolbar
        textField.inputView = pickerView
            textField.font = UIFont.customFontWithSize(size: 16, thickness: "l")
        pickerView.reloadAllComponents()
        textField.becomeFirstResponder()
        
        
    }
    
    
    func showAsFormSheetWithSearch() {
        
        // Show menu with datasource array - PresentationStyle = Formsheet & SearchBar
        var selectionType : SelectionStyle!
        
        if(isMultiSelection){
            selectionType = SelectionStyle.multiple
        }else{
            selectionType = SelectionStyle.single
        }
        let selectionMenu = RSSelectionMenu(selectionStyle: selectionType, dataSource: self.pickerModel) { (cell, object, indexPath) in
            cell.textLabel?.text = object
        }
        
        
        // show selected items
        selectionMenu.setSelectedItems(items: selectedDataArray) { (text,index , selected, selectedItems) in
            self.selectedDataArray = selectedItems
            if(self.isMultiSelection){
                self.textField.text = self.selectedDataArray.joined(separator: ",")
            }else{
                self.textField.text = text
                self.delegate?.pickerSelected(atIndex: index, tag: self.tag)
            }
           
            self.resetError()
        }
        
        // show searchbar with placeholder text and barTintColor
        // Here you'll get search text - when user types in seachbar
        
        selectionMenu.showSearchBar(withPlaceHolder: "Search", barTintColor: UIColor.white.withAlphaComponent(0.3)) { (searchText) -> ([String]) in
            
            return self.pickerModel.filter({ $0.lowercased().contains(searchText.lowercased())})
        }
        
        // show as formsheet
        selectionMenu.show(style: .formSheet, from: self.view.parentViewController())
        
        
    }
    
    
    @objc func donePicker(){
        if(self.pickerModel.count > 0){
            textField.text = self.pickerModel[ self.pickerView.selectedRow(inComponent: 0)]
            let selectedIndexTemp = selectedIndex
            selectedIndex = self.pickerView.selectedRow(inComponent: 0)
            
            if(self.selectedIndex != selectedIndexTemp){
                delegate?.pickerSelected(atIndex: selectedIndex, tag: self.tag)
            }
            errorLabel.text = ""
        }
        self.view.endEditing(true)
    }
    
    @objc func cancelPicker(){
        //cancel button dismiss datepicker dialog
        self.view.endEditing(true)
    }
    
    @IBAction func dropDownBtnPressed(_ sender: Any){
        textField.resignFirstResponder()
        textField.isEnabled = true
        if(isSearchAblePicker){
            self.showAsFormSheetWithSearch()
        }else if(isDatePicker){
            showDatePicker()
        }else if(isDateTimePicker){
            self.showDateTimePicker()
        }else{
           self.showGenericPickerView()
        }
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    
    func xibSetup(){
        backgroundColor = .clear
        view = loadViewFromNib()
        
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.translatesAutoresizingMaskIntoConstraints = true
     //   self.textField.delegate = self
        addSubview(view)
       // self.resetError()
        self.textField.font = UIFont.customFontWithSize(size: 16, thickness: "l")
        self.textLbl.font = UIFont.customFontWithSize(size: 12, thickness: "l")
        self.textField.tintColor = UIColor.init(named: "GreyTextColor")!
        textField.delegate = self
        pickerView.delegate = self
        pickerView.dataSource = self
    }
    
    
    func loadViewFromNib() -> UIView{
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let nibView = nib.instantiate(withOwner: self, options: nil).first as! UIView
        return nibView
        
    }

    func resetError(){
        self.errorLabel.isHidden = true
    }
    
    func setErrorWith(message: String){
    }
    
    func setFieldAsVerified(){
        self.errorIcon.isHidden = false
        self.errorIcon.image = #imageLiteral(resourceName: "text_field_verified")
    }
    
    func setFieldAsNotVerified(){
        self.errorIcon.isHidden = false
        self.errorIcon.image = #imageLiteral(resourceName: "text_field_not_verified")
        self.errorLabel.isHidden = false
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField){
        
        self.resetError()
    }


}

extension EDTextField: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.resetError()
       // self.textLbl.isHidden = false
        return true
    }
 func textFieldDidEndEditing(_ textField: UITextField) {

        if(self.selected_icon != nil){
            if(self.textField.text != nil){
            }
        }

    }
    
}
extension EDTextField: UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if(self.pickerModel.count > 0){
        }
        
    }
}

extension EDTextField: UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return pickerModel.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return pickerModel[row]
        
    }
}
