//
//  EmptyTableViewBackgroundView.swift
//  EDYOU
//
//  Created by Umer Farooq on 05/05/2021.
//  Copyright © 2021 o2geeks. All rights reserved.
//

import UIKit

class EmptyTableViewBackgroundView: UIView {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var messageLabel: UILabel!

    class func instanceFromNib() -> EmptyTableViewBackgroundView {
        return UINib(nibName: "EmptyTableViewBackgroundView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! EmptyTableViewBackgroundView
    }

}
