//
//  RecipeTextField.swift
//  EDYOU
//
//  Created by Umer Farooq on 05/05/2021.
//  Copyright © 2021 o2geeks. All rights reserved.
//

import Foundation
import UIKit

protocol RecipeTextFieldDelegate {
    func pickerSelected(atIndex:Int, tag :Int)
}
class RecipeTextField:UITextField,UITextFieldDelegate{
    var pickerView = UIPickerView()
    var selectedIndex = 0
    var pickerModel = [String]()
    
    var recipeDelegate:RecipeTextFieldDelegate?
    var dropDownButton = UIButton(type: .custom)
    
    
    @IBInspectable var isDropDown: Bool{
        
        get{
            return dropDownButton.isHidden
        }
        
        set(isDropDown){
            if(isDropDown){
                self.tintColor = UIColor.clear
                setupButton()
                dropDownButton.isHidden = false
                
            }else{
                self.isEnabled = true
                dropDownButton.isHidden = true
            }
        }
    }
    func setupButton(){
        let button = UIButton(type: .custom)
        button.setTitle("", for: .normal)
        button.frame = CGRect(origin: CGPoint(x: 0, y: 0), size: self.bounds.size)
        button.autoresizingMask = []
        button.addTarget(self, action: #selector(showGenericPickerView), for: .touchUpInside)
        self.addSubview(button)
        self.dropDownButton = button
    }
    
    func setup() {
        self.delegate = self
        pickerView.delegate = self
        pickerView.dataSource = self
        if(pickerModel.count > 0){
            self.text = pickerModel[selectedIndex]
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    func textFieldShouldReturn(userText: UITextField!) -> Bool {
        self.resignFirstResponder()
        return true;
    }
    
    @objc func showGenericPickerView(){
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        
        //done button & cancel button
        let doneButton = UIBarButtonItem(title: "Select", style: .plain, target: self, action: #selector(RecipeTextField.donePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let titleButton = UIBarButtonItem(title: self.placeholder, style: .plain, target: nil, action: nil)
        titleButton.isEnabled = false
        titleButton.tintColor = UIColor.lightGray
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(RecipeTextField.cancelPicker))
        toolbar.setItems([cancelButton,spaceButton,titleButton,spaceButton,doneButton], animated: false)
        toolbar.backgroundColor = UIColor.white
        self.inputAccessoryView = toolbar
        self.inputView = pickerView
        pickerView.reloadAllComponents()
        self.becomeFirstResponder()
        
        
    }
    
    
    @objc func donePicker(){
        if(self.pickerModel.count > 0){
            self.text = self.pickerModel[ self.pickerView.selectedRow(inComponent: 0)]
            let selectedIndexTemp = selectedIndex
            selectedIndex = self.pickerView.selectedRow(inComponent: 0)
            
            if(self.selectedIndex != selectedIndexTemp){
                recipeDelegate?.pickerSelected(atIndex: selectedIndex, tag: self.tag)
            }
        }
        self.resignFirstResponder()
    }
    
    @objc func cancelPicker(){
        //cancel button dismiss datepicker dialog
        self.resignFirstResponder()
    }
        func textFieldShouldReturn(_ textField: UITextField) -> Bool
        {
            self.resignFirstResponder()
            return true
        }
        
        func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
            // self.textLbl.isHidden = false
            return true
        }
        func textFieldDidEndEditing(_ textField: UITextField) {
            
            print(#function)
            
        }
}
extension RecipeTextField: UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if(self.pickerModel.count > 0){
        }
        
    }
}

extension RecipeTextField: UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return pickerModel.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return pickerModel[row]
        
    }
}
