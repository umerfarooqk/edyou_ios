//
//  BaseViewController.swift
//  EDYOU
//
//  Created by Umer Farooq on 05/05/2021.
//  Copyright © 2021 o2geeks. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView
import RealmSwift
import SwiftMessages

class BaseViewController: UIViewController,NVActivityIndicatorViewable {
    var params = Parameters()
    var leftSearchBarButtonItem : UIBarButtonItem?
    var rightSearchBarButtonItem : UIBarButtonItem?
    var isInternetReachable:Bool{
        get{
            return NetworkReachabilityManager()!.isReachable
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.setHidesBackButton(true, animated: false)
        setNeedsStatusBarAppearanceUpdate()
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        .lightContent
    }
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    func startLoading(_ isRefreshControl: Bool = false){
            let size = CGSize(width: 70, height:70)
        startAnimating(size, message: "", type: NVActivityIndicatorType.circleStrokeSpin)
    }
    
    func stopLoading(){
        stopAnimating()
    }
    
    func setupTransparentNavBar(){
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont.navigationFont(),NSAttributedString.Key.foregroundColor:UIColor.init(named: "GreyTextColor")!]
        self.navigationController?.navigationBar.barStyle = .black
        
        //  UIApplication.shared.statusBarStyle = .default
        navigationController?.navigationBar.isTranslucent = true
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationController?.navigationBar.shadowImage = UIImage()
        
        self.navigationController?.navigationBar.tintColor = UIColor.black
        
        self.navigationItem.leftBarButtonItem = nil
        
    }
    
    func addBackButtonToNavigationBar(){
        let btn = UIBarButtonItem(image: UIImage.init(named: "back_button"), style: UIBarButtonItem.Style.plain, target: self, action: #selector(goBack))
        btn.tintColor = UIColor.init(named: "GreyTextColor")!
        self.leftSearchBarButtonItem = btn
        
        self.navigationItem.leftBarButtonItem = self.leftSearchBarButtonItem
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
    }
    
    @objc func goBack(){
        self.navigationController?.popViewController(animated: true)
    }
    func display_alert(msg_title : String , msg_desc : String ,action_title : String = "Ok" ,completionBlock : @escaping () -> () = {})
    {
        let ac = UIAlertController(title: msg_title, message: msg_desc, preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: action_title, style: .default)
        {
            (result : UIAlertAction) -> Void in
            // _ = Constants.appDelegate.findCurrentViewController().navigationController?.popViewController(animated: true)
            completionBlock()
        })
        self.present(ac, animated: true)
    }
    
    
    func showErrorWith(message: String){
        var config = SwiftMessages.Config()
        config.presentationStyle = .top
        let error = MessageView.viewFromNib(layout: .tabView)
        error.configureTheme(.error)
        error.configureContent(title: "", body: message)
        //error.button?.setTitle("Stop", for: .normal)
        error.button?.isHidden = true
        SwiftMessages.show(config: config, view: error)
        self.view.layoutIfNeeded()
    }
    
    func showMessageSuccessWith(message: String){
        var config = SwiftMessages.Config()
        config.presentationStyle = .top
        let error = MessageView.viewFromNib(layout: .tabView)
        error.configureTheme(.success)
        error.configureContent(title: "", body: message)
        //error.button?.setTitle("Stop", for: .normal)
        error.button?.isHidden = true
        SwiftMessages.show(config: config, view: error)
        
    }
    
//    func getCities(stateID :Int,completionBlock : @escaping () -> () = {}){
//        if let _ = AppManager.sharedInstance.cities.first(where: {$0.state_id == stateID}){
//            completionBlock()
//        }else{
//            self.startLoading()
//            AppManager.sharedInstance.cities.removeAll()
//            let successClosure: DefaultArrayResultAPISuccessClosure = {
//                (result) in
//                let code = ((result["code"] as? Int) ?? 500)
//                self.stopLoading()
//                if code == StatusCode.Success.rawValue{
//                    if let citiesList = result["cities"] as? NSArray{
//                        for item in citiesList {
//                            
//                            let cityObject = CitiesModel(value:item )
//                            AppManager.sharedInstance.cities.append(cityObject)
//                        }
//                    }
//                    
//                    completionBlock()
//                }
//            }
//            let failureClosure: DefaultAPIFailureClosure = {
//                (error) in
//                print(error)
//                self.stopLoading()
//                self.display_alert(msg_title: "Error", msg_desc: "Something went wrong please try again later.")
//            }
//            ApiManager.shared.getStatesCities(parameters: ["state_id" : stateID], success: successClosure, failure: failureClosure)
//        }
//    }
    
}
extension BaseViewController: UIGestureRecognizerDelegate{
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
}
