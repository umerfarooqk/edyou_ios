//
//  Constants.swift
//  EDYOU
//
//  Created by Umer Farooq on 05/05/2021.
//  Copyright © 2021 o2geeks. All rights reserved.
//

import Foundation
import UIKit
struct Constants {
    static let BaseURL = "https://api.edyou.o2geeks.com"
    static let PasswordLength = 6
    
    
    static let BaseImageURL = "http://demos.logichours.com/storage/"
    static let MaxVideoLength = 30 //sec
    static let NoImage = "https://p16-sg-default.akamaized.net/aweme/720x720/tiktok-obj/1662378686264321.jpeg"
    static let appDelegate = AppDelegate()
    static let AuthenticationStoryBoard = "Main"
}
struct ScreenSize{
    ///Width: *Screen width*
    static let Width = UIScreen.main.bounds.width
    ///Height: *Screen Height*
    static let Height = UIScreen.main.bounds.height

}

struct ErrorMessages{
    static let PasswordLength =  "This password is too short"
    
    
    static let InternetIssue = "Please check your internet connection or try again later"
    static let InternalServerError = "Something went wrong. Please try again"
    static let InvalidEmailAddress = "Please enter a valid email address"
    
}
