//
//  Enums.swift
//  EDYOU
//
//  Created by Umer Farooq on 05/05/2021.
//  Copyright © 2021 o2geeks. All rights reserved.
//

import Foundation

enum StatusCode :Int{
   case Success = 200
    case TokenExpire = 401
    case Error = 400
    case DataMissing = 404
    case ServerError = 500
}

enum LoginSource :String{
    case Simple = "simple"
    case Google = "google"
    case Mobile = "mobile"
    case Facebook = "facebook"
    case Apple = "apple"
}
