//
//  Utility.swift
//  EDYOU
//
//  Created by Umer Farooq on 05/05/2021.
//  Copyright © 2021 o2geeks. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift
import SwiftMessages
class Utility {

    class func emptyViewMessageWithImage<T: Equatable>(message:String,image: UIImage? = nil, viewController: UIViewController, tableCollectionView: T) {
        
        let emptyView = EmptyTableViewBackgroundView.instanceFromNib()
        emptyView.imageView.image = image
        emptyView.imageView.tintColor = UIColor.lightGray
        emptyView.messageLabel.text = message
        emptyView.messageLabel.font = UIFont.preferredFont(forTextStyle: .body)
        if let tView = tableCollectionView as? UITableView{
            emptyView.frame = tView.bounds
            tView.backgroundView = emptyView
            tView.separatorStyle = .none
        }else if let cView = tableCollectionView as? UICollectionView{
            emptyView.frame = cView.bounds
            cView.backgroundView = emptyView
        }
    }

    class  func showMessageOnStatusBar(message :String){
        let status2 = MessageView.viewFromNib(layout: .statusLine)
        status2.backgroundView.backgroundColor = UIColor.orange
        status2.bodyLabel?.textColor = UIColor.white
        status2.configureContent(body: message)
        var status2Config = SwiftMessages.defaultConfig
        status2Config.presentationContext = .window(windowLevel: UIWindow.Level.normal)
        status2Config.preferredStatusBarStyle = .lightContent
        SwiftMessages.show(config: status2Config, view: status2)
    }
    
    class  func showSuccessMessage(message :String){
        let status2 = MessageView.viewFromNib(layout: .statusLine)
        //status2.backgroundView.backgroundColor = UIColor.CustomColorFromHexaWithAlpha("22bb33", alpha: 1.0)
        status2.bodyLabel?.textColor = UIColor.white
        status2.configureContent(body: message)
        var status2Config = SwiftMessages.defaultConfig
        status2Config.presentationContext = .window(windowLevel: UIWindow.Level.normal)
        status2Config.preferredStatusBarStyle = .lightContent
        SwiftMessages.show(config: status2Config, view: status2)
    }
    
    class func showFailureMessage(message :String){
        let status2 = MessageView.viewFromNib(layout: .statusLine)
        status2.backgroundView.backgroundColor = UIColor.red
        status2.bodyLabel?.textColor = UIColor.white
        status2.configureContent(body: message)
        var status2Config = SwiftMessages.defaultConfig
        status2Config.presentationContext = .window(windowLevel: UIWindow.Level.normal)
        status2Config.preferredStatusBarStyle = .lightContent
        SwiftMessages.show(config: status2Config, view: status2)
    }
    
}
