//
//  AppDelegate.swift
//  EDYOU
//
//  Created by UmerAfzal on 5/5/21.
//

import UIKit
import CoreData
import AppCenter
import AppCenterAnalytics
import AppCenterCrashes
import IQKeyboardManagerSwift
import RealmSwift
import Alamofire

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    static var kDeviceProportionalHeight:CGFloat = 0
    static var deviceWidth : CGFloat = 0.0

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        AppCenter.start(withAppSecret: "95e6c737-d258-472f-ab93-fcfc1287acb6", services:[
          Analytics.self,
          Crashes.self
        ])
        
        //IQKeyboardManager.shared.enable = true
        if UIScreen.main.bounds.width >= 1024{
            AppDelegate.kDeviceProportionalHeight = ScreenSize.Height / 926.0
        }else{
            AppDelegate.kDeviceProportionalHeight =
                (ScreenSize.Height / 926.0) > 1 ? 1 : ScreenSize.Height / 926.0
        }
        NetworkManager.shared.startNetworkReachabilityObserver()
        AppDelegate.deviceWidth = ScreenSize.Width
       // showTestVC()
        migrateRealm(version: 1)
        
        return true
    }

    func migrateRealm(version : Int)  {
        
        let config = Realm.Configuration(
            schemaVersion: UInt64(version),  // Must be greater than previous version
            migrationBlock: { migration, oldSchemaVersion in
                print("Realm migration did run")  // Log to know migration was executed
        })
        
        // Make sure to set the default configuration
        Realm.Configuration.defaultConfiguration = config
    }
    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "EDYOU")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

}

