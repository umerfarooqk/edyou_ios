//
//  UIImageView+Extention.swift
//  EDYOU
//
//  Created by Umer Farooq on 05/05/2021.
//  Copyright © 2021 o2geeks. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation
let imageCache = NSCache<AnyObject, AnyObject>()
extension UIImageView {


    //load image async from inaternet
    func loadFromURL(photoUrl:String){
        //NSURL
        let url = URL(string: photoUrl)!
        if let imageFromCache = imageCache.object(forKey: photoUrl as AnyObject) {
            image = imageFromCache as? UIImage
            return
        }
        DispatchQueue.global().async { //1
            let asset = AVAsset(url: url) //2
            let avAssetImageGenerator = AVAssetImageGenerator(asset: asset) //3
            avAssetImageGenerator.appliesPreferredTrackTransform = true //4
            let thumnailTime = CMTimeMake(value: 2, timescale: 15) //5
            do {
                let cgThumbImage = try avAssetImageGenerator.copyCGImage(at: thumnailTime, actualTime: nil) //6
                let thumbNailImage = UIImage(cgImage: cgThumbImage) //7
                imageCache.setObject(thumbNailImage, forKey: photoUrl as AnyObject,cost:1)
                DispatchQueue.main.async { //8
                    self.image = thumbNailImage
                }
            } catch {
                print(error.localizedDescription) //10
                DispatchQueue.main.async {
                    
                }
            }
        }
    }


}
