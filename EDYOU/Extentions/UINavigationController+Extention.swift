//
//  UINavigationController+Extention.swift
//  EDYOU
//
//  Created by Umer Farooq on 19/05/2021.
//  Copyright © 2021 o2geeks. All rights reserved.
//

import Foundation
import UIKit
extension UINavigationController {
  func popToAViewController(ofClass: AnyClass, animated: Bool = true) {
    if let vc = viewControllers.last(where: { $0.isKind(of: ofClass) }) {
      popToViewController(vc, animated: animated)
    }
  }
}
extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
