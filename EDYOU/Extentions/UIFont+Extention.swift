//
//  UIFont+Extention.swift
//  EDYOU
//
//  Created by Umer Farooq on 05/05/2021.
//  Copyright © 2021 o2geeks. All rights reserved.
//

import UIKit
import Device

extension UIFont{
    
   class func navigationFont() -> UIFont{
        var size : CGFloat = 14.0
        switch Device.size() {
        case .screen3_5Inch,.screen4Inch:
            size = 14.0
        case .screen4_7Inch:
            size = 16.0
        default:
            size = 18.0
        }
        let scaledSize = size * fontScale(for: size)
        return UIFont(name: "Poppins-Light", size: scaledSize)!
        
}
    
    class func customFontWithSize(size : CGFloat ,thickness : String = "R") -> UIFont{
        var sizeNew : CGFloat = 0.0
        switch Device.size() {
//        case .screen3_5Inch:
//            sizeNew = size - 4
//        case .screen4Inch:
//            sizeNew = size - 3
//        case .screen4_7Inch:
//            sizeNew = size - 2
//        case .screen5_5Inch :
//            sizeNew = size - 1
        default:
            sizeNew = size
        }
        let scaledSize = sizeNew * fontScale(for: sizeNew)
        
        var faimlyName = "Poppins-"
        if(thickness.count < 4) {
            switch thickness.lowercased(){
            case "b":
                faimlyName.append("Bold")
            case "l":
                faimlyName.append("Light")
            case "m":
                faimlyName.append("Medium")
            case "sb":
                faimlyName.append("SemiBold")
            default :
                faimlyName.append("Regular")
            }
        }
        //return UIFont(name: faimlyName, size: scaledSize)!
        return UIFont.systemFont(ofSize: scaledSize)
    }
    
    class func fontScale(for fontSize: CGFloat) -> CGFloat {
        switch UIApplication.shared.preferredContentSizeCategory {
        case UIContentSizeCategory.accessibilityExtraExtraExtraLarge:    return (fontSize + 5) / fontSize
        case UIContentSizeCategory.accessibilityExtraExtraLarge:         return (fontSize + 5) / fontSize
        case UIContentSizeCategory.accessibilityExtraLarge:              return (fontSize + 5) / fontSize
        case UIContentSizeCategory.accessibilityLarge:                   return (fontSize + 5) / fontSize
        case UIContentSizeCategory.accessibilityMedium:                  return (fontSize + 4) / fontSize
        case UIContentSizeCategory.extraExtraExtraLarge:                 return (fontSize + 3) / fontSize
        case UIContentSizeCategory.extraExtraLarge:                      return (fontSize + 2) / fontSize
        case UIContentSizeCategory.extraLarge:                           return (fontSize + 1) / fontSize
        case UIContentSizeCategory.large:                                return 1.0
        case UIContentSizeCategory.medium:                               return (fontSize - 1) / fontSize
        case UIContentSizeCategory.small:                                return (fontSize - 2) / fontSize
        case UIContentSizeCategory.extraSmall:                           return (fontSize - 3) / fontSize
        default:
            return 1.0
        }
    }
}
