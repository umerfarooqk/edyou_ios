//
//  DynamicVerticalConstraint.swift
//  EDYOU
//
//  Created by Umer Farooq on 05/05/2021.
//  Copyright © 2021 o2geeks. All rights reserved.
//

import Foundation
import UIKit

class DynamicVerticalConstraint: NSLayoutConstraint {
    override init() {
        super.init()
        updateConstant()
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        updateConstant()
    }
    //It will send contraint constant according to devices. For this you have to set your contraint according to iPhone 8p
    func updateConstant(){
        self.constant = self.constant *
            AppDelegate.kDeviceProportionalHeight
    }
}

extension UIFont{
    func resizeFont() -> UIFont{
      return  self.withSize(self.pointSize * (AppDelegate.deviceWidth / 428.0))
   
    }

}
