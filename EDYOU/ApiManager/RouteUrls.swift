//
//  RouteUrls.swift
//  EDYOU
//
//  Created by Umer Farooq on 05/05/2021.
//  Copyright © 2021 o2geeks. All rights reserved.
//

import Foundation
enum Route: String {
    case RegisterEmail = "/api/v1/signup"
    case RegisterOTP = "/api/v1/signup/token"
    case RegisterSetName = "/api/v1/user/onboarding/{token}/name"
    case RegisterSetPassword = "/api/v1/user/onboarding/{token}/password"
    case RegisterSetProfilePhoto = "/api/v1/user/onboarding/{token}/profile_photo"
    case RegisterSetEducation = "/api/v1/user/onboarding/{token}/education"
    case RegisterSetAddress = "/api/v1/user/onboarding/{token}/address"
    case RegisterSetPhone = "/api/v1/user/onboarding/{token}/phone"
    case RegisterSetGender = "/api/v1/user/onboarding/{token}/gender_dob"
    case RegisterSetNotification = "/api/v1/user/onboarding/{token}/notification"
    func url() -> URL{
        return URL(string: Constants.BaseURL + self.rawValue.replace(target: "{token}", withString: (SignupSharedManager.instance.onboarding_token ?? "").addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!))!
    }
}
