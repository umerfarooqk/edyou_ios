//
//  SprustApiManager.swift
//  EDYOU
//
//  Created by Umer Farooq on 05/05/2021.
//  Copyright © 2021 o2geeks. All rights reserved.
//

import Foundation
import Alamofire

typealias DefaultAPIFailureClosure = (NSError) -> Void
typealias DefaultAPISuccessClosure = (Dictionary<String,AnyObject>) -> Void
typealias DefaultArrayResultAPIProgress = (Double) -> Void
typealias DefaultBoolResultAPISuccesClosure = (Bool) -> Void
typealias DefaultArrayResultAPISuccessClosure = (Dictionary<String,AnyObject>) -> Void


protocol ACAPIErrorHandler {
    func handleErrorFromResponse(response: Dictionary<String,AnyObject>)
    func handleErrorFromERror(error:NSError)
}

class ApiManager : NSObject{
    static let shared = ApiManager()
    let authentication = AuthenticationApi()
    var serverToken: String? {
        get{
            return AppManager.sharedInstance.accessToken.token ?? ""
        }
    }

}
