//
//  APIManagerBase.swift
//  EDYOU
//
//  Created by Umer Farooq on 05/05/2021.
//  Copyright © 2021 o2geeks. All rights reserved.
//

import Foundation
import Alamofire

class APIManagerBase: NSObject {
    let defaultRequestHeader = ["Content-Type": "application/json"]
    let uploadRequestHeader = ["Content-Type": "application/x-www-form-urlencoded"]
    var isInternetReachable:Bool{
        get{
            return NetworkManager.shared.reachabilityManager!.isReachable
        }
    }
    
    func getAuthorizationHeader () -> Dictionary<String,String>{
        
        if(AppManager.sharedInstance.isUserLoggedIn()){
            if let token = ApiManager.shared.serverToken {
                var str = "Bearer "
                str.append(token)
                return ["Authorization":str,"Accept":"application/json"]
            }
        }
        return ["Content-Type":"application/json"]
    }
    
    
    func GETURLfor(route:URL, parameters: Parameters) -> URL?{
        var queryParameters = ""
        for key in parameters.keys {
            if queryParameters.isEmpty {
                queryParameters =  "?\(key)=\((String(describing: (parameters[key]!))).addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!)"
            } else {
                queryParameters +=  "&\(key)=\((String(describing: (parameters[key]!))).addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!)"
            }
            queryParameters =  queryParameters.trimmingCharacters(in: .whitespaces)
            
        }
        if let components: NSURLComponents = NSURLComponents(string: (route.absoluteString+queryParameters)){
            return components.url! as URL
        }
        return nil
    }
    
    func postRequestWithArray(route: URL,parameters : [Parameters],
                              success:@escaping DefaultArrayResultAPISuccessClosure,
                              failure:@escaping DefaultAPIFailureClosure){
        var request = URLRequest(url: route)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")


        request.httpBody = try! JSONSerialization.data(withJSONObject: parameters)

        Alamofire.request(request)
            .responseJSON { response in
                
                debugPrint(response)
                
                guard response.result.error == nil else{
                    if let err = response.result.error as? URLError, err.code == .notConnectedToInternet {
                        // no internet connection
                        print(err)
                    } else {
                        failure(response.result.error! as NSError)
                    }
                    
                    return;
                }
                
                if let value = response.result.value {
                    print (value)
                    if let jsonResponse = response.result.value as? Dictionary<String, AnyObject>{
                        success(jsonResponse)
                    } else {
                        success(Dictionary<String, AnyObject>())
                    }
                    
                }
                
        }
    }
    func postRequestWith(route: URL,parameters: Parameters,
                         success:@escaping DefaultArrayResultAPISuccessClosure,
                         failure:@escaping DefaultAPIFailureClosure){
        
        
        if(!self.isInternetReachable){
            failure(NSError(domain: Constants.BaseURL, code: 404, userInfo: ["Error":"URL unreachable"]))
            return;
        }

        Alamofire.request(route, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: getAuthorizationHeader ()).responseJSON{
            response in
            
            debugPrint(response)
            
            guard response.result.error == nil else{
                if let err = response.result.error as? URLError, err.code == .notConnectedToInternet {
                    // no internet connection
                    print(err)
                } else {
                    failure(response.result.error! as NSError)
                }
                
                return;
            }
            
            if let value = response.result.value {
                print (value)
                if let jsonResponse = response.result.value as? Dictionary<String, AnyObject>{
                    success(jsonResponse)
                } else {
                    success(Dictionary<String, AnyObject>())
                }
                
            }
            
        }
        
    }
    
    func getRequestWith(route: URL,
                        success:@escaping DefaultArrayResultAPISuccessClosure,
                        failure:@escaping DefaultAPIFailureClosure){
        
        if(!self.isInternetReachable){
            failure(NSError(domain: Constants.BaseURL, code: 404, userInfo: ["Error":"URL Unreatchable"]))
            return;
        }
        Alamofire.request(route, method: .get, encoding: JSONEncoding.prettyPrinted, headers: getAuthorizationHeader ()).responseJSON{
            response in
            
            guard response.result.error == nil else{
                if let err = response.result.error as? URLError, err.code == .notConnectedToInternet {
                    // no internet connection
                    print(err)
                } else {
                    failure(response.result.error! as NSError)
                }
                
                return;
            }
            if response.result.isSuccess {
                if let jsonResponse = response.result.value as? Dictionary<String, AnyObject>{
                    success(jsonResponse)
                } else {
                    success(Dictionary<String, AnyObject>())
                }
            }
        }
        
    }
    func postProfileWithMultipart(route: URL,parameters: Parameters,
                                  success:@escaping DefaultArrayResultAPISuccessClosure,
                                  failure:@escaping DefaultAPIFailureClosure, uploadingProgress: @escaping DefaultArrayResultAPIProgress){
        
        let URLSTR = try! URLRequest(url: route.absoluteString, method: HTTPMethod.post, headers: getAuthorizationHeader())
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            
            var subParameters = Dictionary<String, AnyObject>()
            let keys: Array<String> = Array(parameters.keys)
            let values = Array(parameters.values)
            
            for i in 0..<keys.count {
                //                if ((keys[i] != "file") && (keys[i] != "images")) {
                subParameters[keys[i]] = values[i] as AnyObject
            }
            
            
            for (key, value) in subParameters {
                if let data:Data = value as? Data {
                    let identifier = UUID()
                    multipartFormData.append(data, withName: key, fileName: "\(identifier.uuidString).jpg", mimeType: "image/jpeg")
                } else {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key)
                }
            }
            
        }, with: URLSTR, encodingCompletion: {result in
            
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress { (progress) in
                  print("Fraction Completed \(progress.fractionCompleted)")
                    uploadingProgress(progress.fractionCompleted)
                }
                
                upload.responseJSON { response in
                    
                    guard response.result.error == nil else{
                        
                        print("error in calling post request")
                        return;
                    }
                    
                    
                    
                    if let value = response.result.value {
                        print (value)
                        if let jsonResponse = response.result.value as? Dictionary<String, AnyObject>{
                            success(jsonResponse)
                        } else {
                            success(Dictionary<String, AnyObject>())
                        }
                        
                    }
                    
                }
            case .failure(let encodingError):
                failure(encodingError as NSError)
            }
        })
    }
    func postRequestWithMultipart(route: URL,parameters: Parameters,
                                  success:@escaping DefaultArrayResultAPISuccessClosure,
                                  failure:@escaping DefaultAPIFailureClosure, uploadingProgress: @escaping DefaultArrayResultAPIProgress){
        
        let URLSTR = try! URLRequest(url: route.absoluteString, method: HTTPMethod.post, headers: getAuthorizationHeader())
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            
            var subParameters = Dictionary<String, AnyObject>()
            let keys: Array<String> = Array(parameters.keys)
            let values = Array(parameters.values)
            
            for i in 0..<keys.count {
                //                if ((keys[i] != "file") && (keys[i] != "images")) {
                subParameters[keys[i]] = values[i] as AnyObject
            }
            
            
            for (key, value) in subParameters {
                if let data:Data = value as? Data {
                    let identifier = UUID()
                    multipartFormData.append(data, withName: key, fileName: "\(identifier.uuidString).mp4", mimeType: "video/mp4")
                } else {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key)
                }
            }
            
        }, with: URLSTR, encodingCompletion: {result in
            
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress { (progress) in
                  print("Fraction Completed \(progress.fractionCompleted)")
                    uploadingProgress(progress.fractionCompleted)
                }
                
                upload.responseJSON { response in
                    
                    guard response.result.error == nil else{
                        
                        print("error in calling post request")
                        return;
                    }
                    
                    
                    
                    if let value = response.result.value {
                        print (value)
                        if let jsonResponse = response.result.value as? Dictionary<String, AnyObject>{
                            success(jsonResponse)
                        } else {
                            success(Dictionary<String, AnyObject>())
                        }
                        
                    }
                    
                }
            case .failure(let encodingError):
                failure(encodingError as NSError)
            }
        })
    }
}

