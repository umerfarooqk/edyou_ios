//
//  AuthenticationApi.swift
//  EDYOU
//
//  Created by Umer Farooq on 05/05/2021.
//  Copyright © 2021 o2geeks. All rights reserved.
//

import Foundation
import Alamofire

class AuthenticationApi : APIManagerBase{
    
    func loginWith(parameters: Parameters, success:@escaping DefaultArrayResultAPISuccessClosure,
                           failure:@escaping DefaultAPIFailureClosure){
        
        let routeURL: URL = Route.RegisterEmail.url()
        
        self.postRequestWith(route: routeURL, parameters: parameters, success: success, failure: failure)
    }

    func signUpWithEmail(parameters: Parameters, success:@escaping DefaultArrayResultAPISuccessClosure,
                   failure:@escaping DefaultAPIFailureClosure){
        
        let routeURL: URL =  Route.RegisterEmail.url()
        
        self.postRequestWith(route: routeURL, parameters: parameters, success: success, failure: failure)
    }
    func signUpOTP(parameters: Parameters, success:@escaping DefaultArrayResultAPISuccessClosure,
                   failure:@escaping DefaultAPIFailureClosure){
        
        let routeURL: URL =  Route.RegisterOTP.url()
        
        self.postRequestWith(route: routeURL, parameters: parameters, success: success, failure: failure)
    }
    func signUpSetName(parameters: Parameters, success:@escaping DefaultArrayResultAPISuccessClosure,
                   failure:@escaping DefaultAPIFailureClosure){
        
        let routeURL: URL = Route.RegisterSetName.url()
        
        self.postRequestWith(route: routeURL, parameters: parameters, success: success, failure: failure)
    }
    
    func signUpSetPassword(parameters: Parameters, success:@escaping DefaultArrayResultAPISuccessClosure,
                   failure:@escaping DefaultAPIFailureClosure){
        
        let routeURL: URL = Route.RegisterSetPassword.url()
        
        self.postRequestWith(route: routeURL, parameters: parameters, success: success, failure: failure)
    }
    
    func signUpSetProfile(parameters: Parameters, success:@escaping DefaultArrayResultAPISuccessClosure,
                          failure:@escaping DefaultAPIFailureClosure,progress :@escaping  DefaultArrayResultAPIProgress){
        
        let routeURL: URL = Route.RegisterSetProfilePhoto.url()
        
        self.postProfileWithMultipart(route: routeURL, parameters: parameters, success: success, failure: failure, uploadingProgress: progress)
    }
    func signUpSetEducation(parameters: [Parameters], success:@escaping DefaultArrayResultAPISuccessClosure,
                   failure:@escaping DefaultAPIFailureClosure){
        
        let routeURL: URL = Route.RegisterSetEducation.url()
        
        self.postRequestWithArray(route: routeURL, parameters: parameters, success: success, failure: failure)
    }
    func signUpSetAddress(parameters: Parameters, success:@escaping DefaultArrayResultAPISuccessClosure,
                   failure:@escaping DefaultAPIFailureClosure){
        
        let routeURL: URL = Route.RegisterSetAddress.url()
        
        self.postRequestWith(route: routeURL, parameters: parameters, success: success, failure: failure)
    }
    func signUpSetPhone(parameters: Parameters, success:@escaping DefaultArrayResultAPISuccessClosure,
                   failure:@escaping DefaultAPIFailureClosure){
        
        let routeURL: URL = Route.RegisterSetPhone.url()
        
        self.postRequestWith(route: routeURL, parameters: parameters, success: success, failure: failure)
    }
    func signUpSetGender(parameters: Parameters, success:@escaping DefaultArrayResultAPISuccessClosure,
                   failure:@escaping DefaultAPIFailureClosure){
        
        let routeURL: URL = Route.RegisterSetGender.url()
        
        self.postRequestWith(route: routeURL, parameters: parameters, success: success, failure: failure)
    }
    func signUpSetNotification(parameters: Parameters, success:@escaping DefaultArrayResultAPISuccessClosure,
                   failure:@escaping DefaultAPIFailureClosure){
        
        let routeURL: URL = Route.RegisterSetNotification.url()
        
        self.postRequestWith(route: routeURL, parameters: parameters, success: success, failure: failure)
    }
}
