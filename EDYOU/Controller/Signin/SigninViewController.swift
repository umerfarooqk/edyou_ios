//
//  SigninViewController.swift
//  EDYOU
//
//  Created by UmerAfzal on 5/5/21.
//

import Foundation
import UIKit

class SigninViewController: BaseViewController {
    
    @IBOutlet weak var emailView: EDTextField!
    @IBOutlet weak var passwordView: EDTextField!
    
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var forgotPasswordBtn: UIButton!
    @IBOutlet weak var signUpBtn: UIButton!
    
    @IBOutlet weak var heyLbl: UILabel!
    @IBOutlet weak var credientialLbl: UILabel!
    @IBOutlet weak var forgotLbl: UILabel!
    @IBOutlet weak var signup1Lbl: UILabel!
    @IBOutlet weak var signup2Lbl: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setDesign()
    }
    
    override func viewWillAppear(_ animated: Bool) {
       
    }
    
    //Set Designs
    private func setDesign() {
        self.emailView.layer.borderColor = UIColor.init(named: "InviteLineColor")?.cgColor
        self.passwordView.layer.borderColor = UIColor.init(named: "InviteLineColor")?.cgColor
    }
}
