//
//  WelcomeViewController.swift
//  EDYOU
//
//  Created by UmerAfzal on 5/5/21.
//

import Foundation
import UIKit

class WelcomeViewController: UIViewController {
    @IBOutlet weak var verifiedLbl: UILabel!
    @IBOutlet weak var continueLbl: UILabel!
    @IBOutlet weak var createAccountLbl: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setDesign()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        AppManager.sharedInstance.isUserLoggedIn()
    }
    
    //Set Designs
    private func setDesign() {
        
        verifiedLbl.font = verifiedLbl.font.resizeFont()
        continueLbl.font = continueLbl.font.resizeFont()
        createAccountLbl.font = createAccountLbl.font.resizeFont()
    }
}
