//
//  SetAvatarViewController.swift
//  EDYOU
//
//  Created by UmerAfzal on 5/5/21.
//

import Foundation
import UIKit

class SetAvatarViewController: BaseViewController {
    
    @IBOutlet private weak var profileImgView : UIView!

    @IBOutlet weak var backImage: UIImageView!
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var profileLbl: UILabel!
    @IBOutlet weak var skipBtn: UIButton!
    @IBOutlet weak var profileImage: UIImageView!
    var isImageAdded = false
    @IBOutlet weak var descLbl: UILabel!
    private lazy var imagePicker: ImagePicker = {
         let imagePicker = ImagePicker()
         imagePicker.delegate = self
         return imagePicker
     }()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setDesign()
    }
    
    override func viewWillAppear(_ animated: Bool) {
       
    }
    @IBAction func setImageAction(_ sender: UIButton) {

            let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)

            let photo = UIAlertAction(
                title: "Take photo",
                style: UIAlertAction.Style.default) { (action) in
                self.imagePicker.cameraAsscessRequest()
            }
        alertController.addAction(photo)

            let photoLibrary = UIAlertAction(
                title: "Photo library",
                style: UIAlertAction.Style.default) { (action) in
                self.imagePicker.photoGalleryAsscessRequest()
            }
        alertController.addAction(photoLibrary)
            alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))

            if UIDevice.current.userInterfaceIdiom == .pad {
                alertController.popoverPresentationController?.sourceView = sender
                alertController.popoverPresentationController?.sourceRect = sender.bounds
                alertController.popoverPresentationController?.permittedArrowDirections = [.down, .up]
            }

            self.present(alertController, animated: true)
    }
    
    //Set Designs
    private func setDesign() {
        self.profileImgView.layer.borderColor = UIColor.init(named: "InviteLineColor")?.cgColor
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(backButtonTapped))
        backImage!.addGestureRecognizer(tapGestureRecognizer)
        nextBtn.titleLabel?.font = nextBtn.titleLabel?.font.resizeFont()
        skipBtn.titleLabel?.font = skipBtn.titleLabel?.font.resizeFont()
        descLbl.font = descLbl.font.resizeFont()
        profileLbl.font = profileLbl.font.resizeFont()
    }
    
    @objc func backButtonTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func nextBtnAction(_ sender: UIButton) {
        if(!isImageAdded){
            let storyBoard : UIStoryboard = UIStoryboard(name: Constants.AuthenticationStoryBoard, bundle:nil)
            let inviteViewController = storyBoard.instantiateViewController(withIdentifier: "InviteViewController") as! InviteViewController
            self.navigationController?.pushViewController(inviteViewController, animated: true)
            return
        }
        let params = ["file" : profileImage.image!.jpegData(compressionQuality: 0.6)!]
        self.startLoading()
        let successClosure: DefaultArrayResultAPISuccessClosure = {
            (result) in
            print(result)
            self.stopLoading()
            var message = ((result["message"] as? String) ?? "")
            if(message == ""){
                message = ((result["detail"] as? String) ?? "")
            }
            if(message != ""){
            self.display_alert(msg_title: "Error", msg_desc: message)
            }else{
                
                let storyBoard : UIStoryboard = UIStoryboard(name: Constants.AuthenticationStoryBoard, bundle:nil)
                let inviteViewController = storyBoard.instantiateViewController(withIdentifier: "InviteViewController") as! InviteViewController
                self.navigationController?.pushViewController(inviteViewController, animated: true)
            }

        }
        let failureClosure: DefaultAPIFailureClosure = {
            (error) in
            print(error)
            self.stopLoading()
            self.display_alert(msg_title: "Error", msg_desc: ErrorMessages.InternalServerError)
        }
        
        ApiManager.shared.authentication.signUpSetProfile(parameters: params, success: successClosure, failure: failureClosure) { (progress) in
            // Progress here
            
        }
    }
    
}
extension SetAvatarViewController : ImagePickerDelegate{
    func imagePickerDelegate(canUseCamera accessIsAllowed: Bool, delegatedForm: ImagePicker) {
        delegatedForm.present(parent: self, sourceType: .camera)
    }
    
    func imagePickerDelegate(canUseGallery accessIsAllowed: Bool, delegatedForm: ImagePicker) {
        delegatedForm.present(parent: self, sourceType: .photoLibrary)
    }
    
    func imagePickerDelegate(didSelect image: UIImage, delegatedForm: ImagePicker, indexPath: IndexPath?) {
        profileImage.contentMode = .scaleAspectFit
        profileImage.image = image
        isImageAdded = true
        imagePicker.dismiss()
    }
    
    func imagePickerDelegate(didCancel delegatedForm: ImagePicker) {
        imagePicker.dismiss()
    }
    
    
}
