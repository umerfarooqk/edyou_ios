//
//  EmailViewController.swift
//  EDYOU
//
//  Created by UmerAfzal on 5/5/21.
//

import Foundation
import UIKit
import SwiftValidator
import Alamofire

class EmailViewController: BaseViewController {
    
    @IBOutlet weak var emailView: EDTextField!
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var signInBtn: UIButton!
    
    @IBOutlet weak var emailLbl: UILabel!
    
    var validator: Validator!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setDesign()
        self.setupValidator()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
       
    }
    @IBAction func nextBtnAction(_ sender: UIButton) {
        self.resetErrors()
        if(NetworkManager.shared.reachabilityManager!.isReachable){
        validator.validate(self)
        }else{
            self.showErrorWith(message: ErrorMessages.InternetIssue)
        }
    }
    
    //Set Designs
    private func setDesign() {
        self.emailView.layer.borderColor = UIColor.init(named: "InviteLineColor")?.cgColor
        nextBtn.titleLabel?.font = nextBtn.titleLabel?.font.resizeFont()
        emailLbl.font = emailLbl.font.resizeFont()
        signInBtn.titleLabel?.font = signInBtn.titleLabel?.font.resizeFont()
    }
    
    private func setupValidator(){
        validator = Validator()
        
        validator.registerField(emailView.textField,errorLabel: emailView.errorLabel, rules: [RequiredRule(),EmailRule(message: ErrorMessages.InvalidEmailAddress)])
    }
    func resetErrors(){
        self.emailView.resetError()
    }
    private func callEmailApi(){
        let params = ["email":emailView.textField.text ?? ""]
        self.startLoading()
        let successClosure: DefaultArrayResultAPISuccessClosure = {
            (result) in
            print(result)
            self.stopLoading()
            if ((result["success"] as? Bool) ?? false){
                
                SignupSharedManager.instance.onetime_token = ((result["data"]?["onetime_token"] as? String) ?? "")
                SignupSharedManager.instance.email = ((result["data"]?["email"] as? String) ?? "")
                
                let storyBoard : UIStoryboard = UIStoryboard(name: Constants.AuthenticationStoryBoard, bundle:nil)
                let OTPController = storyBoard.instantiateViewController(withIdentifier: "OTPViewController") as! OTPViewController
                self.navigationController?.pushViewController(OTPController, animated: true)
            }else{
                var message = ((result["message"] as? String) ?? "")
                if(message == ""){
                    message = ((result["detail"] as? String) ?? "")
                }
                self.display_alert(msg_title: "Error", msg_desc: message)
            }

        }
        let failureClosure: DefaultAPIFailureClosure = {
            (error) in
            print(error)
            self.stopLoading()
            self.display_alert(msg_title: "Error", msg_desc: ErrorMessages.InternalServerError)
        }
        ApiManager.shared.authentication.signUpWithEmail(parameters: params, success: successClosure, failure: failureClosure)

    }
    
}

extension EmailViewController : ValidationDelegate {
    func validationSuccessful() {
        if(NetworkManager.shared.reachabilityManager!.isReachable){
        callEmailApi()
        }
    }
    func validationFailed(_ errors: [(Validatable, ValidationError)]) {
        for (field, error) in errors {
            if let field = field as? UITextField {
            }

            print(error.errorMessage)
            error.errorLabel?.text = error.errorMessage // works if you added labels
            error.errorLabel?.isHidden = false
        }
    }
    
}
