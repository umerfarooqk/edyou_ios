//
//  InviteViewController.swift
//  EDYOU
//
//  Created by UmerAfzal on 5/5/21.
//

import Foundation
import UIKit

class InviteViewController: BaseViewController {
    
    @IBOutlet private weak var socialView : UIView!
    @IBOutlet private weak var smsView : UIView!
    @IBOutlet private weak var linkView : UIView!
    @IBOutlet private weak var moreView : UIView!
    @IBOutlet weak var backImage: UIImageView!
    @IBOutlet weak var inviteLbl: UILabel!
    
    @IBOutlet weak var skipBtn: UIButton!
    @IBOutlet weak var nextBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setDesign()
    }
    
    override func viewWillAppear(_ animated: Bool) {
       
    }
    
    //Set Designs
    private func setDesign() {
        self.socialView.layer.borderColor = UIColor.init(named: "InviteLineColor")?.cgColor
        self.smsView.layer.borderColor = UIColor.init(named: "InviteLineColor")?.cgColor
        self.linkView.layer.borderColor = UIColor.init(named: "InviteLineColor")?.cgColor
        self.moreView.layer.borderColor = UIColor.init(named: "InviteLineColor")?.cgColor
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(backButtonTapped))
        backImage!.addGestureRecognizer(tapGestureRecognizer)
        nextBtn.titleLabel?.font = nextBtn.titleLabel?.font.resizeFont()
        skipBtn.titleLabel?.font = nextBtn.titleLabel?.font.resizeFont()
    }
    
    @objc func backButtonTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func nextBtnAction(_ sender: UIButton) {
    }
    @IBAction func fbAction(_ sender: UIButton) {
    }
    @IBAction func twitterAction(_ sender: UIButton) {
    }
    @IBAction func gMailAction(_ sender: UIButton) {
    }
    @IBAction func whatsAppAction(_ sender: UIButton) {
    }
    @IBAction func smsAction(_ sender: UIButton) {
    }
    @IBAction func copyLinkAction(_ sender: UIButton) {
    }
    @IBAction func moreAction(_ sender: UIButton) {
    }
}
