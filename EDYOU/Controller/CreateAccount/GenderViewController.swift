//
//  GenderViewController.swift
//  EDYOU
//
//  Created by UmerAfzal on 5/5/21.
//

import Foundation
import UIKit
import SwiftValidator

class GenderViewController: BaseViewController {
    
    @IBOutlet private weak var dobView : EDTextField!
    @IBOutlet private weak var genderView : EDTextField!

    @IBOutlet weak var genderLbl: UILabel!
    @IBOutlet weak var skipBtn: UIButton!
    @IBOutlet weak var backImage: UIImageView!
    @IBOutlet weak var descLbl: UILabel!
    @IBOutlet weak var nextBtn: UIButton!
    var validator: Validator!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setDesign()
        self.setupValidator()
    }
    
    override func viewWillAppear(_ animated: Bool) {
       
    }
    private func setupValidator(){
        validator = Validator()
        
        validator.registerField(dobView.textField,errorLabel: dobView.errorLabel,rules:
                                    [RequiredRule()])
        validator.registerField(genderView.textField, errorLabel: genderView.errorLabel, rules: [RequiredRule()])
    }
    
    func resetErrors(){
        self.dobView.resetError()
        self.genderView.resetError()
    }
    //Set Designs
    private func setDesign() {
        self.dobView.layer.borderColor = UIColor.init(named: "InviteLineColor")?.cgColor
        self.genderView.layer.borderColor = UIColor.init(named: "InviteLineColor")?.cgColor
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(backButtonTapped))
        backImage!.addGestureRecognizer(tapGestureRecognizer)
        nextBtn.titleLabel?.font = nextBtn.titleLabel?.font.resizeFont()
        genderLbl.font = genderLbl.font.resizeFont()
        descLbl.font = descLbl.font.resizeFont()
        skipBtn.titleLabel?.font = skipBtn.titleLabel?.font.resizeFont()
        genderView.pickerModel = ["Male", "Female","Neuter"]
        dobView.minDate = Calendar.current.date(byAdding: .year, value: -120, to: Date())!
        
    }
    private func callGenderApi(){
        let params = ["gender" : genderView.textField.text ?? "" , "date_of_birth" : dobView.textField.text ?? ""]
        self.startLoading()
        let successClosure: DefaultArrayResultAPISuccessClosure = {
            (result) in
            print(result)
            self.stopLoading()
            var message = ((result["message"] as? String) ?? "")
            if(message == ""){
                message = ((result["detail"] as? String) ?? "")
            }
            if(message != ""){
            self.display_alert(msg_title: "Error", msg_desc: message)
            }else{
                
                let storyBoard : UIStoryboard = UIStoryboard(name: Constants.AuthenticationStoryBoard, bundle:nil)
                let notificationViewController = storyBoard.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
                self.navigationController?.pushViewController(notificationViewController, animated: true)
            }

        }
        let failureClosure: DefaultAPIFailureClosure = {
            (error) in
            print(error)
            self.stopLoading()
            self.display_alert(msg_title: "Error", msg_desc: ErrorMessages.InternalServerError)
        }
        ApiManager.shared.authentication.signUpSetGender(parameters: params, success: successClosure, failure: failureClosure)

    }
    @objc func backButtonTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func nextBtnAction(_ sender: UIButton) {
        self.resetErrors()
        if(NetworkManager.shared.reachabilityManager!.isReachable){
        validator.validate(self)
        }else{
            self.showErrorWith(message: ErrorMessages.InternetIssue)
        }
    }
    
}
extension GenderViewController : ValidationDelegate {
    func validationSuccessful() {
        if(NetworkManager.shared.reachabilityManager!.isReachable){
            callGenderApi()
        }
    }
    func validationFailed(_ errors: [(Validatable, ValidationError)]) {
        for (field, error) in errors {
            if let field = field as? UITextField {
            }

            print(error.errorMessage)
            error.errorLabel?.text = error.errorMessage // works if you added labels
            error.errorLabel?.isHidden = false
        }
    }
    
}
