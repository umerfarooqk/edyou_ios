//
//  SignupSharedManager.swift
//  EDYOU
//
//  Created by Umer Farooq on 16/05/2021.
//  Copyright © 2021 o2geeks. All rights reserved.
//

import Foundation
class SignupSharedManager: NSObject {
    
    
    static let instance = SignupSharedManager()
    var onetime_token:String? = ""
    var onboarding_token:String? = ""
    var email:String? = ""
    
}
