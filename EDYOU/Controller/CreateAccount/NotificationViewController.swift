//
//  NotificationViewController.swift
//  EDYOU
//
//  Created by UmerAfzal on 5/5/21.
//

import Foundation
import UIKit

class NotificationViewController: BaseViewController {
    
    @IBOutlet weak var descLbl: UILabel!
    @IBOutlet weak var notificationLbl: UILabel!
    @IBOutlet weak var skipBtn: UIButton!
    @IBOutlet weak var backImage: UIImageView!
    @IBOutlet weak var nextBtn: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setDesign()
    }
    
    override func viewWillAppear(_ animated: Bool) {
       
    }
    
    //Set Designs
    private func setDesign() {
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(backButtonTapped))
        backImage!.addGestureRecognizer(tapGestureRecognizer)
        nextBtn.titleLabel?.font = nextBtn.titleLabel?.font.resizeFont()
        descLbl.font = descLbl.font.resizeFont()
        notificationLbl.font = notificationLbl.font.resizeFont()
        skipBtn.titleLabel?.font = skipBtn.titleLabel?.font.resizeFont()
    }
    
    @objc func backButtonTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func notifyBtnAction(_ sender: UIButton) {
        
        callNotificationApi()
    }
    private func callNotificationApi(){
        let params = ["notification_enabled" : true]
        self.startLoading()
        let successClosure: DefaultArrayResultAPISuccessClosure = {
            (result) in
            print(result)
            self.stopLoading()
            var message = ((result["message"] as? String) ?? "")
            if(message == ""){
                message = ((result["detail"] as? String) ?? "")
            }
            if(message != ""){
            self.display_alert(msg_title: "Error", msg_desc: message)
            }else{
                
                self.navigationController?.popToAViewController(ofClass: WelcomeViewController.self, animated: true)
            }

        }
        let failureClosure: DefaultAPIFailureClosure = {
            (error) in
            print(error)
            self.stopLoading()
            self.display_alert(msg_title: "Error", msg_desc: ErrorMessages.InternalServerError)
        }
        ApiManager.shared.authentication.signUpSetNotification(parameters: params, success: successClosure, failure: failureClosure)

    }
}
