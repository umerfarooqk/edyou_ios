//
//  ForgotViewController.swift
//  EDYOU
//
//  Created by Umer Farooq on 11/05/2021.
//  Copyright © 2021 o2geeks. All rights reserved.
//

import UIKit

class ForgotViewController: BaseViewController {

    @IBOutlet weak var forgotLbl: UILabel!
    @IBOutlet weak var forgotDescLbl: UILabel!
    
    @IBOutlet weak var emailField: EDTextField!
    @IBOutlet weak var backImage: UIImageView!
    
    @IBOutlet weak var resetBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setDesign()
    }
    
    override func viewWillAppear(_ animated: Bool) {
       
    }
    @IBAction func resetPassword(_ sender: UIButton) {
    }
    
    //Set Designs
    private func setDesign() {
        self.emailField.layer.borderColor = UIColor.init(named: "InviteLineColor")?.cgColor
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(backButtonTapped))
        backImage!.addGestureRecognizer(tapGestureRecognizer)
        resetBtn.titleLabel?.font = resetBtn.titleLabel?.font.resizeFont()
        forgotLbl.font = forgotLbl.font.resizeFont()
        forgotDescLbl.font = forgotDescLbl.font.resizeFont()
    }
    
    @objc func backButtonTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func resetBtnAction(_ sender: UIButton) {
    }
    
}
