//
//  NameViewController.swift
//  EDYOU
//
//  Created by UmerAfzal on 5/5/21.
//

import Foundation
import UIKit
import SwiftValidator

class NameViewController: BaseViewController {
    
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet private weak var firstNameView : EDTextField!
    @IBOutlet private weak var lastNameView : EDTextField!
    @IBOutlet weak var backImage: UIImageView!
    @IBOutlet weak var nextBtn: UIButton!
    var validator: Validator!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setDesign()
        self.setupValidator()
    }
    
    override func viewWillAppear(_ animated: Bool) {
       
    }
    
    //Set Designs
    private func setDesign() {
        self.firstNameView.layer.borderColor = UIColor.init(named: "InviteLineColor")?.cgColor
        self.lastNameView.layer.borderColor = UIColor.init(named: "InviteLineColor")?.cgColor
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(backButtonTapped))
        backImage!.addGestureRecognizer(tapGestureRecognizer)
        nextBtn.titleLabel?.font = nextBtn.titleLabel?.font.resizeFont()
        nameLbl.font = nameLbl.font.resizeFont()
    }
    private func setupValidator(){
        validator = Validator()
        
        validator.registerField(firstNameView.textField,errorLabel: firstNameView.errorLabel, rules: [RequiredRule()])
        validator.registerField(lastNameView.textField,errorLabel: lastNameView.errorLabel, rules: [RequiredRule()])
    }
    @objc func backButtonTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    func resetErrors(){
        self.firstNameView.resetError()
        self.lastNameView.resetError()
    }
    @IBAction func nextBtnAction(_ sender: UIButton) {
        self.resetErrors()
        if(NetworkManager.shared.reachabilityManager!.isReachable){
        validator.validate(self)
        }else{
            self.showErrorWith(message: ErrorMessages.InternetIssue)
        }
    }
    private func callNameApi(){
        let params = ["first_name" : firstNameView.textField.text ?? "", "last_name" : lastNameView.textField.text ?? "", "middle_name" : "", "nick_name" : ""]
        self.startLoading()
        let successClosure: DefaultArrayResultAPISuccessClosure = {
            (result) in
            print(result)
            self.stopLoading()
            if ((result["success"] as? Bool) ?? false){
                
                let storyBoard : UIStoryboard = UIStoryboard(name: Constants.AuthenticationStoryBoard, bundle:nil)
                let passwordViewController = storyBoard.instantiateViewController(withIdentifier: "PasswordViewController") as! PasswordViewController
                self.navigationController?.pushViewController(passwordViewController, animated: true)
            }else{
                var message = ((result["message"] as? String) ?? "")
                if(message == ""){
                    message = ((result["detail"] as? String) ?? "")
                }
                self.display_alert(msg_title: "Error", msg_desc: message)
            }

        }
        let failureClosure: DefaultAPIFailureClosure = {
            (error) in
            print(error)
            self.stopLoading()
            self.display_alert(msg_title: "Error", msg_desc: ErrorMessages.InternalServerError)
        }
        ApiManager.shared.authentication.signUpSetName(parameters: params, success: successClosure, failure: failureClosure)

    }
}
extension NameViewController : ValidationDelegate {
    func validationSuccessful() {
        if(NetworkManager.shared.reachabilityManager!.isReachable){
            callNameApi()
        }
    }
    func validationFailed(_ errors: [(Validatable, ValidationError)]) {
        for (field, error) in errors {
            if let field = field as? UITextField {
            }

            print(error.errorMessage)
            error.errorLabel?.text = error.errorMessage // works if you added labels
            error.errorLabel?.isHidden = false
        }
    }
    
}
