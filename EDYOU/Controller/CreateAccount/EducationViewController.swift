//
//  EducationViewController.swift
//  EDYOU
//
//  Created by UmerAfzal on 5/5/21.
//

import Foundation
import UIKit
import SwiftValidator
import Alamofire

class EducationViewController: BaseViewController {
    
    @IBOutlet private weak var uniView : EDTextField!
    @IBOutlet private weak var degreeView : EDTextField!
    @IBOutlet private weak var fieldView : EDTextField!
    @IBOutlet private weak var startView : EDTextField!
    @IBOutlet private weak var endView : EDTextField!
    @IBOutlet weak var backImage: UIImageView!
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var universityLbl: UILabel!
    @IBOutlet weak var descLbl: UILabel!
    @IBOutlet weak var skipBtn: UIButton!
    var validator: Validator!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setDesign()
        setupValidator()
    }
    
    override func viewWillAppear(_ animated: Bool) {
       
    }
    private func setupValidator(){
        validator = Validator()
        
        validator.registerField(uniView.textField,errorLabel: uniView.errorLabel, rules: [RequiredRule()])
        validator.registerField(degreeView.textField,errorLabel: degreeView.errorLabel, rules: [RequiredRule()])
        validator.registerField(fieldView.textField,errorLabel: fieldView.errorLabel, rules: [RequiredRule()])
        validator.registerField(startView.textField,errorLabel: startView.errorLabel, rules: [RequiredRule()])
        validator.registerField(endView.textField,errorLabel: endView.errorLabel, rules: [RequiredRule()])

    }
    //Set Designs
    private func setDesign() {
        self.uniView.layer.borderColor = UIColor.init(named: "InviteLineColor")?.cgColor
        self.degreeView.layer.borderColor = UIColor.init(named: "InviteLineColor")?.cgColor
        self.fieldView.layer.borderColor = UIColor.init(named: "InviteLineColor")?.cgColor
        self.startView.layer.borderColor = UIColor.init(named: "InviteLineColor")?.cgColor
        self.endView.layer.borderColor = UIColor.init(named: "InviteLineColor")?.cgColor
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(backButtonTapped))
        backImage!.addGestureRecognizer(tapGestureRecognizer)
        nextBtn.titleLabel?.font = nextBtn.titleLabel?.font.resizeFont()
        skipBtn.titleLabel?.font = skipBtn.titleLabel?.font.resizeFont()
        universityLbl.font = universityLbl.font.resizeFont()
        descLbl.font = descLbl.font.resizeFont()
        uniView.pickerModel = ["A University","B University","C University","D University","E University"]
        degreeView.pickerModel = ["A Degree","B Degree","C Degree","D Degree","E Degree"]
        fieldView.pickerModel = ["A Field","B Field","C Field","D Field","E Field"]
        startView.pickerModel = ["2021","2020","2019","2018","2017","2016","2015"]
        endView.pickerModel = ["On going","2020","2019","2018","2017","2016","2015"]
    }
    
    @objc func backButtonTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func nextBtnAction(_ sender: UIButton) {
        self.resetErrors()
        if(NetworkManager.shared.reachabilityManager!.isReachable){
        validator.validate(self)
        }else{
            self.showErrorWith(message: ErrorMessages.InternetIssue)
        }
    }
    func resetErrors(){
        self.uniView.resetError()
        self.degreeView.resetError()
        self.fieldView.resetError()
        self.startView.resetError()
        self.endView.resetError()
    }
    
    private func callEducationApi(){
        
        
        let params = [["institute_name" : uniView.textField.text ?? "" ,"degree_name":degreeView.textField.text ?? "","degree_field_name" : fieldView.textField.text ?? "" , "degree_start" :startView.textField.text ?? "", "degree_end" : endView.textField.text ?? ""]]
        self.startLoading()
        let successClosure: DefaultArrayResultAPISuccessClosure = {
            (result) in
            print(result)
            self.stopLoading()
            var message = ((result["message"] as? String) ?? "")
            if(message == ""){
                message = ((result["detail"] as? String) ?? "")
            }
            if(message != ""){
            self.display_alert(msg_title: "Error", msg_desc: message)
            }else{
                
                let storyBoard : UIStoryboard = UIStoryboard(name: Constants.AuthenticationStoryBoard, bundle:nil)
                let genderViewController = storyBoard.instantiateViewController(withIdentifier: "GenderViewController") as! GenderViewController
                self.navigationController?.pushViewController(genderViewController, animated: true)
            }

        }
        let failureClosure: DefaultAPIFailureClosure = {
            (error) in
            print(error)
            self.stopLoading()
            self.display_alert(msg_title: "Error", msg_desc: ErrorMessages.InternalServerError)
        }
        ApiManager.shared.authentication.signUpSetEducation(parameters: params, success: successClosure, failure: failureClosure)

    }
}
extension EducationViewController : ValidationDelegate {
    func validationSuccessful() {
        if(NetworkManager.shared.reachabilityManager!.isReachable){
            callEducationApi()
        }
    }
    func validationFailed(_ errors: [(Validatable, ValidationError)]) {
        for (field, error) in errors {
            if let field = field as? UITextField {
            }

            print(error.errorMessage)
            error.errorLabel?.text = error.errorMessage // works if you added labels
            error.errorLabel?.isHidden = false
        }
    }
    
}
