//
//  OTPViewController.swift
//  EDYOU
//
//  Created by UmerAfzal on 5/5/21.
//

import Foundation
import UIKit

class OTPViewController: BaseViewController {
    
    @IBOutlet weak var otpView: UIView!
    var verificationCode = ""
    var allFieldsFilled: Bool = false
    var loadResetScreen:Bool = true
    
    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var firstCodeTextField: UITextField!
    @IBOutlet weak var secondCodeTextField: UITextField!
    @IBOutlet weak var thirdCodeTextField: UITextField!
    @IBOutlet weak var forthCodeTextField: UITextField!
    
    @IBOutlet weak var timerLbl: UILabel!
    
    @IBOutlet weak var separatorOne: UIView!
    @IBOutlet weak var separatorTwo: UIView!
    @IBOutlet weak var separatorThree: UIView!
    @IBOutlet weak var backImage: UIImageView!
    
    @IBOutlet weak var verifyEmailLbl: UILabel!
    @IBOutlet weak var nextBtn: UIButton!
    var counter = 30
    var minute = 1
    var timer: Timer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setDesign()
        self.setOTPView()
        emailLbl.text = "Please enter the 4 digit code sent to\n\(SignupSharedManager.instance.email ?? "")"
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    deinit {
        self.timer?.invalidate()
        timer = nil
    }
    //Set Designs
    private func setDesign() {
        self.otpView.layer.borderColor = UIColor.init(named: "InviteLineColor")?.cgColor
        
        self.separatorOne.backgroundColor = UIColor.init(named: "InviteLineColor")!
        
        self.separatorTwo.backgroundColor = UIColor.init(named: "InviteLineColor")!
        self.separatorThree.backgroundColor = UIColor.init(named: "InviteLineColor")!
        self.emailLbl.textColor = UIColor.init(named: "GreyTextColor")!
        self.timerLbl.textColor = UIColor.init(named: "GreyTextColor")!
        
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(backButtonTapped))
        backImage!.addGestureRecognizer(tapGestureRecognizer)
        
        //Resize Fonts
        nextBtn.titleLabel?.font = nextBtn.titleLabel?.font.resizeFont()
        verifyEmailLbl.font = verifyEmailLbl.font.resizeFont()
        emailLbl.font = emailLbl.font.resizeFont()
    }
    @IBAction func confirmBtnAction(_ sender: UIButton) {
        var code = ""
        if(firstCodeTextField.text ?? "" != "" ){
            code = (firstCodeTextField.text ?? "")
        }else{
            return
        }
        if(secondCodeTextField.text ?? "" != "" ){
            code = code +  (secondCodeTextField.text ?? "")
        }else{
            return
        }
        if(thirdCodeTextField.text ?? "" != "" ){
            code = code +  (thirdCodeTextField.text ?? "")
        }else{
            return
        }
        if(forthCodeTextField.text ?? "" != "" ){
            code = code +  (forthCodeTextField.text ?? "")
        }else{
           return
        }
        
        self.verificationCode = code
        
        if(NetworkManager.shared.reachabilityManager!.isReachable){
            callVerificationApi()
        }else{
            self.showErrorWith(message: ErrorMessages.InternetIssue)
        }
 
    }
    private func callVerificationApi(){
        let params = ["code" : verificationCode , "token" : SignupSharedManager.instance.onetime_token ?? ""]
        self.startLoading()
        let successClosure: DefaultArrayResultAPISuccessClosure = {
            (result) in
            print(result)
            self.stopLoading()
            if ((result["success"] as? Bool) ?? false){
                
                SignupSharedManager.instance.onboarding_token = ((result["data"]?["onboarding_token"] as? String) ?? "")
                
                let storyBoard : UIStoryboard = UIStoryboard(name: Constants.AuthenticationStoryBoard, bundle:nil)
                let nameViewController = storyBoard.instantiateViewController(withIdentifier: "NameViewController") as! NameViewController
                self.navigationController?.pushViewController(nameViewController, animated: true)
            }else{
                var message = ((result["message"] as? String) ?? "")
                if(message == ""){
                    message = ((result["detail"] as? String) ?? "")
                }
                self.display_alert(msg_title: "Error", msg_desc: message)
            }

        }
        let failureClosure: DefaultAPIFailureClosure = {
            (error) in
            print(error)
            self.stopLoading()
            self.display_alert(msg_title: "Error", msg_desc: ErrorMessages.InternalServerError)
        }
        ApiManager.shared.authentication.signUpOTP(parameters: params, success: successClosure, failure: failureClosure)

    }
    //MARK: - Setupview
    func setOTPView() {

        firstCodeTextField.delegate = self
        secondCodeTextField.delegate = self
        thirdCodeTextField.delegate = self
        forthCodeTextField.delegate = self
        
        firstCodeTextField.isEnabled = true
        firstCodeTextField.becomeFirstResponder()
        nextBtn.isEnabled = false
        
        self.firstCodeTextField.attributedPlaceholder = NSAttributedString(string:self.firstCodeTextField.placeholder != nil ? self.firstCodeTextField.placeholder! : firstCodeTextField.placeholder!, attributes:[NSAttributedString.Key(rawValue: NSAttributedString.Key.foregroundColor.rawValue): UIColor.init(named: "InviteLineColor")!
            ] as [NSAttributedString.Key: UIColor])
        
        self.secondCodeTextField.attributedPlaceholder = NSAttributedString(string:self.secondCodeTextField.placeholder != nil ? self.secondCodeTextField.placeholder! : secondCodeTextField.placeholder!, attributes:[NSAttributedString.Key(rawValue: NSAttributedString.Key.foregroundColor.rawValue): UIColor.init(named: "InviteLineColor")!
            ] as [NSAttributedString.Key: UIColor])
        
        self.thirdCodeTextField.attributedPlaceholder = NSAttributedString(string:self.thirdCodeTextField.placeholder != nil ? self.thirdCodeTextField.placeholder! : thirdCodeTextField.placeholder!, attributes:[NSAttributedString.Key(rawValue: NSAttributedString.Key.foregroundColor.rawValue): UIColor.init(named: "InviteLineColor")!
            ] as [NSAttributedString.Key: UIColor])
        
        self.forthCodeTextField.attributedPlaceholder = NSAttributedString(string:self.forthCodeTextField.placeholder != nil ? self.forthCodeTextField.placeholder! : forthCodeTextField.placeholder!, attributes:[NSAttributedString.Key(rawValue: NSAttributedString.Key.foregroundColor.rawValue): UIColor.init(named: "InviteLineColor")!
            ] as [NSAttributedString.Key: UIColor])
        
        self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(OTPViewController.setupTimerLabel), userInfo: nil, repeats: true)
    }
    @objc func setupTimerLabel(){
        var timerString : String!

        //for _ in 0...3{
        var second = String(counter)
        if counter == 0{
            counter = 59
            minute -= 1
        }
        if counter < 10{
            second = "0\(counter)"
        }
        let str = "0\(minute):\(second)"
        if counter >= 0 && minute != -1 {
            timerString =  str
            counter -= 1
            //resendBtn.isEnabled = false
        }
        else{
            timerString = "00:00"
            //resendBtn.isEnabled = true
        }
        //
        self.timerLbl.text = timerString
        
    }
    @objc func backButtonTapped() {
        self.navigationController?.popViewController(animated: true)
    }

}

//MARK: - UITextField Delegates
extension OTPViewController:UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.layer.borderColor = UIColor.blue.cgColor
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.layer.borderColor = UIColor.darkGray.cgColor
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if ((textField.text!.count) < 1) && (string.count > 0) {
            
            if textField == firstCodeTextField {
                secondCodeTextField.isEnabled = true
                secondCodeTextField.becomeFirstResponder()
            }
            if textField == secondCodeTextField {
                thirdCodeTextField.isEnabled = true
                thirdCodeTextField.becomeFirstResponder()
            }
            if textField == thirdCodeTextField {
                forthCodeTextField.isEnabled = true
                forthCodeTextField.becomeFirstResponder()
               nextBtn.isEnabled = true
            }
            textField.text = string
            return false
        }
            
        else if ((textField.text?.count)! >= 1 && (string.count == 0)) {
            
            if textField == secondCodeTextField {
                firstCodeTextField.becomeFirstResponder()
              nextBtn.isEnabled = true
            }
            if textField == thirdCodeTextField {
                secondCodeTextField.becomeFirstResponder()
            }
            if textField == forthCodeTextField {
                thirdCodeTextField.becomeFirstResponder()
            }
            textField.text = ""
            
            return false
        }
            
        else if (textField.text?.count)! >= 1 {
            textField.text = string
            return false
        }
        
        return true
    }
}
