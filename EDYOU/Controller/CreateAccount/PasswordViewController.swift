//
//  PasswordViewController.swift
//  EDYOU
//
//  Created by UmerAfzal on 5/5/21.
//

import Foundation
import UIKit
import SwiftValidator

class PasswordViewController: BaseViewController {
    
    @IBOutlet private weak var passwordView : EDTextField!
    @IBOutlet private weak var confirmView : EDTextField!
    @IBOutlet weak var backImage: UIImageView!
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var passwordLbl: UILabel!
    var validator: Validator!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setDesign()
        self.setupValidator()
    }
    
    override func viewWillAppear(_ animated: Bool) {

    }
    private func setupValidator(){
        validator = Validator()
        
        validator.registerField(passwordView.textField,errorLabel: passwordView.errorLabel,rules:
                                    [RequiredRule(),MinLengthRule(length: Constants.PasswordLength, message:ErrorMessages.PasswordLength)])
        validator.registerField(confirmView.textField, errorLabel: confirmView.errorLabel, rules: [RequiredRule(),MinLengthRule(length: Constants.PasswordLength, message:ErrorMessages.PasswordLength),ConfirmationRule(confirmField:passwordView.textField)])
    }
    
    func resetErrors(){
        self.passwordView.resetError()
        self.confirmView.resetError()
    }
    //Set Designs
    private func setDesign() {
        self.passwordView.layer.borderColor = UIColor.init(named: "InviteLineColor")?.cgColor
        self.confirmView.layer.borderColor = UIColor.init(named: "InviteLineColor")?.cgColor
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(backButtonTapped))
        backImage!.addGestureRecognizer(tapGestureRecognizer)
        nextBtn.titleLabel?.font = nextBtn.titleLabel?.font.resizeFont()
        passwordLbl.font = passwordLbl.font.resizeFont()
    }
    
    @objc func backButtonTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func nextBtnAction(_ sender: UIButton) {
        self.resetErrors()
        if(NetworkManager.shared.reachabilityManager!.isReachable){
        validator.validate(self)
        }else{
            self.showErrorWith(message: ErrorMessages.InternetIssue)
        }
    }
    private func callPasswordApi(){
        let params = ["password" : passwordView.textField.text ?? ""]
        self.startLoading()
        let successClosure: DefaultArrayResultAPISuccessClosure = {
            (result) in
            print(result)
            self.stopLoading()
            
            
            var message = ((result["message"] as? String) ?? "")
            if(message == ""){
                message = ((result["detail"] as? String) ?? "")
            }
            if(message != ""){
            self.display_alert(msg_title: "Error", msg_desc: message)
            }else{
                let storyBoard : UIStoryboard = UIStoryboard(name: Constants.AuthenticationStoryBoard, bundle:nil)
                let avatarViewController = storyBoard.instantiateViewController(withIdentifier: "SetAvatarViewController") as! SetAvatarViewController
                self.navigationController?.pushViewController(avatarViewController, animated: true)
            }

        }
        let failureClosure: DefaultAPIFailureClosure = {
            (error) in
            print(error)
            self.stopLoading()
            self.display_alert(msg_title: "Error", msg_desc: ErrorMessages.InternalServerError)
        }
        ApiManager.shared.authentication.signUpSetPassword(parameters: params, success: successClosure, failure: failureClosure)

    }
}
extension PasswordViewController : ValidationDelegate {
    func validationSuccessful() {
        if(NetworkManager.shared.reachabilityManager!.isReachable){
            callPasswordApi()
        }
    }
    func validationFailed(_ errors: [(Validatable, ValidationError)]) {
        for (field, error) in errors {
            if let field = field as? UITextField {
            }

            print(error.errorMessage)
            error.errorLabel?.text = error.errorMessage // works if you added labels
            error.errorLabel?.isHidden = false
        }
    }
    
}
