//
//  AppManager.swift
//  EDYOU
//
//  Created by Umer Farooq on 05/05/2021.
//  Copyright © 2021 o2geeks. All rights reserved.
//

import Foundation
import RealmSwift
class AppManager: NSObject {
    
    
    static let sharedInstance = AppManager()
    var loggedInUser: UserModel!
    var accessToken : TokenModel!
    
    var realm: Realm!
    override init() {
        
        super.init()
        
        if(!(realm != nil)){
            realm = try! Realm()
        }
        
        loggedInUser = realm.objects(UserModel.self).first
        accessToken = realm.objects(TokenModel.self).first
        
        
        loggedInUser = UserModel()
        loggedInUser.email = "umer.farooq@yandex.com"
        loggedInUser.country = "Pakistan"
        loggedInUser.city = "Rawalpindi"
        loggedInUser.id = 23
        
    let users =   try?  JSONEncoder().encode(loggedInUser)
        
        print(String(data: users! , encoding: .utf8)!) // to string Object
        
    let dictionary = try? JSONSerialization.jsonObject(with: users!, options: [])
        print(dictionary!) // to dictionary
        
    }
    
    func isUserLoggedIn() -> Bool{
        loggedInUser = realm.objects(UserModel.self).first
        if (self.loggedInUser) != nil {
            if self.loggedInUser.isInvalidated {
                return false
            }
            return true
        }
        return false
    }
    
    func isGuestUser() -> Bool{
        var guest = false
        if(self.loggedInUser) != nil{
            if(self.loggedInUser.id == 0){
                guest = true
            }else{
                guest = false
            }
        }
        
        return guest
    }
    
    
    func markUserLogout(){
         loggedInUser = realm.objects(UserModel.self).first
        try!  self.realm.write(){
            
            if(loggedInUser == nil){
            }else{
                self.realm.delete(self.loggedInUser)
                self.realm.delete(self.accessToken)
                self.loggedInUser = nil
            }

        }
        
    }
}
