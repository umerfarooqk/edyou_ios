//
//  UserModel.swift
//  EDYOU
//
//  Created by Umer Farooq on 05/05/2021.
//  Copyright © 2021 o2geeks. All rights reserved.
//
import Realm
import RealmSwift

class UserModel: Object,Codable {
    @objc dynamic var id = 0
    @objc dynamic var email : String?
    @objc dynamic var phone : String?
    @objc dynamic var user_name : String?
    dynamic var followers = RealmOptional<Int>()
    dynamic var following   = RealmOptional<Int>()
    @objc dynamic var img_url : String?
    @objc dynamic var created_at : String?
    @objc dynamic var status = 0
    @objc dynamic var city : String?
    @objc dynamic var zip_code : String?
    @objc dynamic var state : String?
    @objc dynamic var country : String?
    @objc dynamic var fcm_token : String?
    override static func primaryKey() -> String? {
        return "id"
    }
}

class TokenModel : Object
{
    @objc dynamic var id = 1
    @objc dynamic var token_type : String?
    @objc dynamic var expires_at : String?
    @objc dynamic var token : String? = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
